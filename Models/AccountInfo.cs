﻿using System;
using System.Collections.Generic;

namespace LibraryManagement.Models
{
    public partial class AccountInfo
    {
        public int AccountId { get; set; }
        public string Name { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }

        public virtual Account Account { get; set; }
    }
}
