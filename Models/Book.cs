﻿using System;
using System.Collections.Generic;

namespace LibraryManagement.Models
{
    public partial class Book
    {
        public Book()
        {
            Bookings = new HashSet<Booking>();
        }

        public int Id { get; set; }
        public string BookName { get; set; }
        public string AuthorName { get; set; }
        public string? Description { get; set; }
        public string? Image { get; set; }

        public virtual Inventory Inventory { get; set; }
        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
