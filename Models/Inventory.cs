﻿using System;
using System.Collections.Generic;

namespace LibraryManagement.Models
{
    public partial class Inventory
    {
        public int BookId { get; set; }
        public int TotalQuantity { get; set; }
        public int QuantityInStock { get; set; }

        public virtual Book Book { get; set; }
    }
}
