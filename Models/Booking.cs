﻿using System;
using System.Collections.Generic;

namespace LibraryManagement.Models
{
    public partial class Booking
    {
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Status { get; set; }
        public int Quantity { get; set; }
        public int? AccountId { get; set; }
        public int? BookId { get; set; }

        public virtual Account Account { get; set; }
        public virtual Book Book { get; set; }
    }
}
