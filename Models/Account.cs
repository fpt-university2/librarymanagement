﻿using System;
using System.Collections.Generic;

namespace LibraryManagement.Models
{
    public partial class Account
    {
        public Account()
        {
            Bookings = new HashSet<Booking>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }

        public virtual Role Role { get; set; }
        public virtual AccountInfo AccountInfo { get; set; }
        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
