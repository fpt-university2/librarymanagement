﻿using LibraryManagement.DAO;
using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Repository
{
    public class AccountRepository : IAccountRepository
    {
        public IEnumerable<Account> GetAll() => AccountDAO.Instance.GetAccountList();

        public Account GetByEmail(string email) => AccountDAO.Instance.GetAccountByEmail(email);

        public void DeleteAccount(Account account) => AccountDAO.Instance.Remove(account);

        public void InsertAccount(Account account) => AccountDAO.Instance.AddNew(account);

        public void UpdateAccount(Account account) => AccountDAO.Instance.Update(account);
    }
}
