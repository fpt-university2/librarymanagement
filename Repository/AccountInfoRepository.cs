﻿using LibraryManagement.Models;
using LibraryManagement.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Repository
{
    public class AccountInfoRepository : IAccountInfoRepository
    {

        public void UpdateAccountInfo(AccountInfo accountInfo) => AccountInfoDAO.Instance.Update(accountInfo);

        public void DeleteAccountInfo(AccountInfo accountInfo) => AccountInfoDAO.Instance.Remove(accountInfo);

        public void CreateAccountInfo(AccountInfo accountInfo) => AccountInfoDAO.Instance.AddNew(accountInfo);

        public IEnumerable<AccountInfo> GetAll() => AccountInfoDAO.Instance.GetAccountInfoList();

        public AccountInfo GetByAccountId(int accountId) => AccountInfoDAO.Instance.GetAccountInfoByAccountId(accountId);
    }
}
