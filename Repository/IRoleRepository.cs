﻿using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Repository
{
    public interface IRoleRepository
    {
        IEnumerable<Role> GetAll();
        Role GetById(int roleId);
    }
}
