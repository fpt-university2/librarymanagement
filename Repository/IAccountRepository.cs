﻿using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Repository
{
    public interface IAccountRepository
    {
        IEnumerable<Account> GetAll();
        Account GetByEmail(string email);
        void InsertAccount(Account account);

        void DeleteAccount(Account account);

        void UpdateAccount(Account account);
    }
}
