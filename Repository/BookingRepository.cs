﻿using LibraryManagement.DAO;
using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Repository
{
    public class BookingRepository : IBookingRepository
    {
        public IEnumerable<Booking> GetAll() => BookingDAO.Instance.GetBookingList();

        public Booking GetById(int id) => BookingDAO.Instance.GetBookingByBookingId(id);
        public void DeleteBooking(Booking booking) => BookingDAO.Instance.Remove(booking);

        public void InsertBooking(Booking booking) => BookingDAO.Instance.AddNew(booking);

        public void UpdateBooking(Booking booking) => BookingDAO.Instance.Update(booking);
    }
}
