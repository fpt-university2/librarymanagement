﻿using LibraryManagement.DAO;
using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Repository
{
    public class RoleRepository : IRoleRepository
    {
        public Role GetById(int roleId) => RoleDAO.Instance.GetRoleByRoleId(roleId);

        public IEnumerable<Role> GetAll() => RoleDAO.Instance.GetRoleList();
    }
}
