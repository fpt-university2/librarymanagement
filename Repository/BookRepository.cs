﻿using LibraryManagement.DAO;
using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Repository
{
    public class BookRepository : IBookRepository
    {
        public IEnumerable<Book> GetAll() => BookDAO.Instance.GetBookList();

        public Book GetById(int id) => BookDAO.Instance.GetBookByBookId(id);
        public void DeleteBook(Book book) => BookDAO.Instance.Remove(book);

        public void InsertBook(Book book) => BookDAO.Instance.AddNew(book);

        public void UpdateBook(Book book) => BookDAO.Instance.Update(book);
    }
}
