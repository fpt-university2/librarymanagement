﻿using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Repository
{
    public interface IInventoryRepository
    {
        IEnumerable<Inventory> GetAll();
        Inventory GetByBookId(int bookId);
        void InsertInventory(Inventory inventory);

        void DeleteInventory(Inventory inventory);

        void UpdateInventory(Inventory inventory);
    }
}
