﻿using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Repository
{
    public interface IAccountInfoRepository
    {
        IEnumerable<AccountInfo> GetAll();
        AccountInfo GetByAccountId(int accountId);
        void UpdateAccountInfo(AccountInfo accountInfo);
        void DeleteAccountInfo(AccountInfo accountInfo);
        void CreateAccountInfo(AccountInfo accountInfo);
    }
}
