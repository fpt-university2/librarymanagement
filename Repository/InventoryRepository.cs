﻿using LibraryManagement.DAO;
using LibraryManagement.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LibraryManagement.Repository
{
    public class InventoryRepository : IInventoryRepository
    {
        public void DeleteInventory(Inventory inventory) => InventoryDAO.Instance.Remove(inventory);

        public IEnumerable<Inventory> GetAll() => InventoryDAO.Instance.GetInventoryList();

        public Inventory GetByBookId(int bookId) => InventoryDAO.Instance.GetInventoryByBookId(bookId);

        public void InsertInventory(Inventory inventory) => InventoryDAO.Instance.AddNew(inventory);

        public void UpdateInventory(Inventory inventory) => InventoryDAO.Instance.Update(inventory);
    }
}
