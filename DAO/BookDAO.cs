﻿using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.DAO
{
    public class BookDAO
    {
        //Using Singleton Pattern
        private static BookDAO instance = null;
        private static readonly object instanceLock = new object();
        private BookDAO() { }
        public static BookDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                        instance = new BookDAO();
                }
                return instance;
            }
        }

        public IEnumerable<Book> GetBookList()
        {
            List<Book> books;
            try
            {
                var _db = new LibraryManagementContext();
                books = _db.Books.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return books;
        }

        public Book GetBookByBookId(int bookId)
        {
            Book book = null;
            try
            {
                var _db = new LibraryManagementContext();
                book = _db.Books.FirstOrDefault(b => b.Id == bookId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return book;
        }

        public void AddNew(Book book)
        {
            try
            {
                // write logic here
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Update(Book book)
        {
            try
            {
                Book _book = GetBookByBookId(book.Id);
                if (_book != null)
                {
                    var _db = new LibraryManagementContext();
                    _book.BookName = book.BookName;
                    _book.AuthorName = book.AuthorName;
                    _book.Description = book.Description;
                    _book.Image = book.Image;

                    _db.SaveChanges();
                }
                else
                {
                    throw new Exception("The book does not already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove(Book book)
        {
            try
            {
                Book _book = GetBookByBookId(book.Id);
                if (_book != null)
                {
                    var _db = new LibraryManagementContext();
                    _db.Books.Remove(_book);
                    _db.SaveChanges();
                }

                else
                {
                    throw new Exception("The book does not already exist");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
