﻿using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.DAO
{
    public class InventoryDAO
    {
        //Using Singleton Pattern
        private static InventoryDAO instance = null;
        private static readonly object instanceLock = new object();
        private InventoryDAO() { }
        public static InventoryDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                        instance = new InventoryDAO();
                }
                return instance;
            }
        }

        public IEnumerable<Inventory> GetInventoryList()
        {
            List<Inventory> inventories;
            try
            {
                var _db = new LibraryManagementContext();
                inventories = _db.Inventories.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return inventories;
        }

        public Inventory GetInventoryByBookId(int bookId)
        {
            Inventory inventory = null;
            try
            {
                var _db = new LibraryManagementContext();
                inventory = _db.Inventories.FirstOrDefault(b => b.BookId == bookId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return inventory;
        }

        public void AddNew(Inventory inventory)
        {
            try
            {
                // write logic here
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Update(Inventory inventory)
        {
            try
            {
                Inventory _inventory = GetInventoryByBookId(inventory.BookId);
                if (_inventory != null)
                {
                    var _db = new LibraryManagementContext();
                    // write logic here
                    _db.SaveChanges();
                }
                else
                {
                    throw new Exception("The inventory does not already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove(Inventory inventory)
        {
            try
            {
                Inventory _inventory = GetInventoryByBookId(inventory.BookId);
                if (_inventory != null)
                {
                    var _db = new LibraryManagementContext();
                    _db.Inventories.Remove(_inventory);
                    _db.SaveChanges();
                }

                else
                {
                    throw new Exception("The inventory does not already exist");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
