﻿using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.DAO
{
    public class AccountDAO
    {
        //Using Singleton Pattern
        private static AccountDAO instance = null;
        private static readonly object instanceLock = new object();
        private AccountDAO() { }
        public static AccountDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                        instance = new AccountDAO();
                }
                return instance;
            }
        }

        public IEnumerable<Account> GetAccountList()
        {
            List<Account> accounts;
            try
            {
                var _db = new LibraryManagementContext();
                accounts = _db.Accounts.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return accounts;
        }

        public Account GetAccountByAccountId(int accountId)
        {
            Account account = null;
            try
            {
                var _db = new LibraryManagementContext();
                account = _db.Accounts.FirstOrDefault(a => a.Id == accountId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return account;
        }

        public Account GetAccountByEmail(string email)
        {
            Account account = null;
            try
            {
                var _db = new LibraryManagementContext();
                account = _db.Accounts.FirstOrDefault(a => email.Equals(a.Email));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return account;
        }

        public void AddNew(Account account)
        {
            try
            {
                Account _account = GetAccountByEmail(account.Email);
                if (_account == null)
                {
                    var _db = new LibraryManagementContext();
                    _db.Accounts.Add(account);
                    _db.SaveChanges();
                }
                else
                {
                    throw new Exception("The account is already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        // update password only
        public void Update(Account account)
        {
            try
            {
                Account _account = GetAccountByEmail(account.Email);
                if (_account != null)
                {
                    var _db = new LibraryManagementContext();
                    _account.Password = account.Password;

                    _db.SaveChanges();
                }
                else
                {
                    throw new Exception("The account does not already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove(Account account)
        {
            try
            {
                Account _account = GetAccountByEmail(account.Email);
                if (_account != null)
                {
                    var _db = new LibraryManagementContext();
                    _db.Accounts.Remove(_account);
                    _db.SaveChanges();
                }

                else
                {
                    throw new Exception("The account does not already exist");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
