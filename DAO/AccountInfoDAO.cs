﻿using LibraryManagement.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.DAO
{
    public class AccountInfoDAO
    {
        //Using Singleton Pattern
        private static AccountInfoDAO instance = null;
        private static readonly object instanceLock = new object();
        private AccountInfoDAO() { }
        public static AccountInfoDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                        instance = new AccountInfoDAO();
                }
                return instance;
            }
        }

        public IEnumerable<AccountInfo> GetAccountInfoList()
        {
            List<AccountInfo> accountInfos;
            try
            {
                var _db = new LibraryManagementContext();
                accountInfos = _db.AccountInfos.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return accountInfos;
        }

        public AccountInfo GetAccountInfoByAccountId(int accountId)
        {
            AccountInfo accountInfo = null;
            try
            {
                var _db = new LibraryManagementContext();
                accountInfo = _db.AccountInfos.FirstOrDefault(a => a.AccountId == accountId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return accountInfo;
        }

        public void AddNew(AccountInfo accountInfo)
        {
            try
            {
                AccountInfo _accountInfo = GetAccountInfoByAccountId(accountInfo.AccountId);
                if (_accountInfo == null)
                {
                    var _db = new LibraryManagementContext();
                    _db.AccountInfos.Add(accountInfo);
                    _db.SaveChanges();
                }
                else
                {
                    throw new Exception("The accountInfo is already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }

        public void Update(AccountInfo accountInfo)
        {
            try
            {
                AccountInfo _accountInfo = GetAccountInfoByAccountId(accountInfo.AccountId);
                if (_accountInfo != null)
                {
                    var _db = new LibraryManagementContext();
                    _accountInfo.Name = accountInfo.Name;
                    _accountInfo.Phone = accountInfo.Phone;
                    _accountInfo.Address = accountInfo.Address;

                    _db.SaveChanges();
                }
                else
                {
                    throw new Exception("The accountInfo does not already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove(AccountInfo accountInfo)
        {
            try
            {
                AccountInfo _accountInfos = GetAccountInfoByAccountId(accountInfo.AccountId);
                if (_accountInfos != null)
                {
                    var _db = new LibraryManagementContext();
                    _db.AccountInfos.Remove(_accountInfos);
                    _db.SaveChanges();
                }

                else
                {
                    throw new Exception("The accountInfo does not already exist");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
