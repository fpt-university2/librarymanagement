﻿using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.DAO
{
    public class RoleDAO
    {
        //Using Singleton Pattern
        private static RoleDAO instance = null;
        private static readonly object instanceLock = new object();
        private RoleDAO() { }
        public static RoleDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                        instance = new RoleDAO();
                }
                return instance;
            }
        }

        public IEnumerable<Role> GetRoleList()
        {
            List<Role> roles;
            try
            {
                var _db = new LibraryManagementContext();
                roles = _db.Roles.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return roles;
        }

        public Role GetRoleByRoleId(int roleId)
        {
            Role role = null;
            try
            {
                var _db = new LibraryManagementContext();
                role = _db.Roles.FirstOrDefault(b => b.Id == roleId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return role;
        }

    }
}
