﻿using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.DAO
{
    public class BookingDAO
    {
        //Using Singleton Pattern
        private static BookingDAO instance = null;
        private static readonly object instanceLock = new object();
        private BookingDAO() { }
        public static BookingDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                        instance = new BookingDAO();
                }
                return instance;
            }
        }

        public IEnumerable<Booking> GetBookingList()
        {
            List<Booking> bookings;
            try
            {
                var _db = new LibraryManagementContext();
                bookings = _db.Bookings.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bookings;
        }

        public Booking GetBookingByBookingId(int bookingId)
        {
            Booking booking = null;
            try
            {
                var _db = new LibraryManagementContext();
                booking = _db.Bookings.FirstOrDefault(b => b.Id == bookingId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return booking;
        }

        public void AddNew(Booking booking)
        {
            try
            {
                // write logic here
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Update(Booking booking)
        {
            try
            {
                Booking _booking = GetBookingByBookingId(booking.Id);
                if (_booking != null)
                {
                    var _db = new LibraryManagementContext();
                    // write logic here
                }
                else
                {
                    throw new Exception("The booking does not already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove(Booking booking)
        {
            try
            {
                Booking _booking = GetBookingByBookingId(booking.Id);
                if (_booking != null)
                {
                    var _db = new LibraryManagementContext();
                    _db.Bookings.Remove(_booking);
                    _db.SaveChanges();
                }

                else
                {
                    throw new Exception("The booking does not already exist");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
